package com.br.cartoes.pagamento.models.dto;

import java.time.LocalDate;

public class PagarCartaoResponse {

    private Double valorPago;

    private LocalDate pagoEm;

    public Double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    public LocalDate getPagoEm() {
        return pagoEm;
    }

    public void setPagoEm(LocalDate pagoEm) {
        this.pagoEm = pagoEm;
    }
}
