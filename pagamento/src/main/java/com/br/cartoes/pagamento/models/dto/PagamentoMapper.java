package com.br.cartoes.pagamento.models.dto;

import com.br.cartoes.pagamento.cartao.Cartao;
import com.br.cartoes.pagamento.models.Pagamento;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PagamentoMapper {

    public Pagamento toPagamento(CriarPagamentoRequest criarPagamentoRequest){
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(criarPagamentoRequest.getDescricao());
        pagamento.setValor(criarPagamentoRequest.getValor());
        pagamento.setCartaoId(criarPagamentoRequest.getCartaoId());

        return pagamento;
    }

    public CriarPagamentoResponse toCriarPagamentoResponse(Pagamento pagamento){
        CriarPagamentoResponse criarPagamentoResponse = new CriarPagamentoResponse();
        criarPagamentoResponse.setId(pagamento.getId());
        criarPagamentoResponse.setCartaoId(pagamento.getCartaoId());
        criarPagamentoResponse.setDescricao(pagamento.getDescricao());
        criarPagamentoResponse.setValor(pagamento.getValor());

        return criarPagamentoResponse;
    }


    public List<GetPagamentoResponse> ToGetPagamentoResponse(List<Pagamento> listarPagamentosPorIdCartao) {

        List<GetPagamentoResponse> listaGetPagamentoResponse = new ArrayList<>();
        GetPagamentoResponse getPagamentoResponse;

        for (Pagamento pagamento : listarPagamentosPorIdCartao) {
            getPagamentoResponse = new GetPagamentoResponse();

            getPagamentoResponse.setId(pagamento.getId());
            getPagamentoResponse.setCartaoId(pagamento.getCartaoId());
            getPagamentoResponse.setDescricao(pagamento.getDescricao());
            getPagamentoResponse.setValor(pagamento.getValor());

            listaGetPagamentoResponse.add(getPagamentoResponse);
        }

        return listaGetPagamentoResponse;
    }
}
