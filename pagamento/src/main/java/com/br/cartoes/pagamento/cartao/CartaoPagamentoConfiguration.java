package com.br.cartoes.pagamento.cartao;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CartaoPagamentoConfiguration {

    @Bean
    public ErrorDecoder getCartaoPagamentoDecoder(){
        return new CartaoPagamentoDecoder();
    }

}
