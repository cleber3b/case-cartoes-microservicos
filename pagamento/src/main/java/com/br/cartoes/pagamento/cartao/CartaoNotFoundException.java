package com.br.cartoes.pagamento.cartao;

import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cartão não encontrado para efetuar o pagamento.")
public class CartaoNotFoundException extends RuntimeException {

}
