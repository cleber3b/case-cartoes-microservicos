package com.br.cartoes.pagamento.cartao;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoPagamentoDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new CartaoNotFoundException(); //lança minha exceção
        }
        return errorDecoder.decode(s, response);
        //nesse caso qualquer coisa do erro acima 404 vai assumir como default o erroDecoder do Feign

    }
}
