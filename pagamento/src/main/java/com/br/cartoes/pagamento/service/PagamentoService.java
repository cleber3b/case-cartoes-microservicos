package com.br.cartoes.pagamento.service;

import com.br.cartoes.pagamento.models.Pagamento;
import com.br.cartoes.pagamento.models.dto.PagarCartaoResponse;
import com.br.cartoes.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    public Pagamento salvarPagamento(Pagamento pagamento) {
        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> listarPagamentosPorIdCartao(int idCartao) {
        return pagamentoRepository.findAllByCartaoId(idCartao);

    }

    public PagarCartaoResponse pagarCartao(int idCartao) {

        PagarCartaoResponse pagarCartaoResponse = new PagarCartaoResponse();

        List<Pagamento> listaPagamentos = pagamentoRepository.findAllByCartaoId(idCartao);
        pagarCartaoResponse.setValorPago(this.somarValoresDosPagamentos(listaPagamentos));
        pagarCartaoResponse.setPagoEm(LocalDate.now());

        pagamentoRepository.deleteAll(listaPagamentos);

        return pagarCartaoResponse;
    }

    private Double somarValoresDosPagamentos(List<Pagamento> pagamentos) {
        Double valorSomado = 0.0;

        for (Pagamento pagamento : pagamentos) {
            valorSomado += pagamento.getValor();
        }

        return valorSomado;
    }
}
