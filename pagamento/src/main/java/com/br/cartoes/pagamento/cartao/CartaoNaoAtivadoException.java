package com.br.cartoes.pagamento.cartao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "Cartão não está liberado para pagamentos.")
public class CartaoNaoAtivadoException extends RuntimeException {
}
