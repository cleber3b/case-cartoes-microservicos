package com.br.cartoes.pagamento.cartao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CARTAO", configuration = CartaoPagamentoConfiguration.class)
public interface CartaoPagamento {

    @GetMapping("/cartao/{idCartao}")
    Cartao buscarCartaoPorId(@PathVariable int idCartao);
}
