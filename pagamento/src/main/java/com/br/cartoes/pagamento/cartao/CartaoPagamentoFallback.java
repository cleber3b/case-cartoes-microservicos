package com.br.cartoes.pagamento.cartao;

public class CartaoPagamentoFallback implements CartaoPagamento {

    /*Aqui apos o implements vc define a regra de negócio e ação que vai acontecer
     * quando o serviço de Cliente for chamado e nõa tiver nenhuma resposta
     * */

    @Override
    public Cartao buscarCartaoPorId(int idCartao) {
        throw new CartaoNotAvaliable();
    }




}
