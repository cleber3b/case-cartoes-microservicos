package com.br.cartoes.pagamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Pagamento não processado.")
public class PagamentoNotFoundException extends RuntimeException {
}
