package com.br.cartoes.pagamento.cartao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE, reason = "MicroServiço de Cartão fora do ar.")
public class CartaoNotAvaliable extends RuntimeException {
}
