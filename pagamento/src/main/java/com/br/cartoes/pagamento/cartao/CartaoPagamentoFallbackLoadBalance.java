package com.br.cartoes.pagamento.cartao;

import com.netflix.client.ClientException;

public class CartaoPagamentoFallbackLoadBalance implements CartaoPagamento {

    private Exception exception;

    public CartaoPagamentoFallbackLoadBalance(Exception exception){
        this.exception = exception;
    }

    @Override
    public Cartao buscarCartaoPorId(int idCartao) {
        if(exception.getCause() instanceof ClientException){
            throw new CartaoNotAvaliable();
        }
        throw (RuntimeException) exception;
    }


}
