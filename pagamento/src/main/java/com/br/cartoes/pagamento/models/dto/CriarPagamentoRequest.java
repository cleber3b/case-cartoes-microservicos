package com.br.cartoes.pagamento.models.dto;

import javax.validation.constraints.*;

public class CriarPagamentoRequest {

    private int cartaoId;

    @NotBlank(message = "Descricao precisa ser preenchida.")
    @NotNull(message = "Descricao precisa ser preenchida.")
    @Size(min = 5, message = "Descricao precisa ser preenchida com no mínio 5 letras.")
    private String descricao;

    @NotNull(message = "Valor não pode ser nulo ou vazio.")
    @DecimalMin(value = "1.0", message = "Valor precisa ser maior que R$1.00.")
    @Digits(integer = 6, fraction = 2, message = "Valor fora do padrão.")
    private Double valor;

    public int getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(int cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
