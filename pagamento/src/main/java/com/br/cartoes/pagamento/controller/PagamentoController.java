package com.br.cartoes.pagamento.controller;

import com.br.cartoes.pagamento.cartao.Cartao;
import com.br.cartoes.pagamento.cartao.CartaoPagamento;
import com.br.cartoes.pagamento.cartao.CartaoNaoAtivadoException;
import com.br.cartoes.pagamento.cartao.CartaoNotFoundException;
import com.br.cartoes.pagamento.exception.PagamentoNotFoundException;
import com.br.cartoes.pagamento.models.Pagamento;
import com.br.cartoes.pagamento.models.dto.*;
import com.br.cartoes.pagamento.service.PagamentoService;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @Autowired
    CartaoPagamento cartaoPagamento;

    @Autowired
    PagamentoMapper pagamentoMapper;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public CriarPagamentoResponse cadastrarPagamento(@RequestBody @Valid CriarPagamentoRequest criarPagamentoRequest) {

        Cartao cartao;

        try {
            cartao = cartaoPagamento.buscarCartaoPorId(criarPagamentoRequest.getCartaoId());

            if (!cartao.getAtivo()) {
                throw new CartaoNaoAtivadoException();
            }

        } catch (FeignException.NotFound ex) {
            throw new CartaoNotFoundException();
        }

        Pagamento pagamento = pagamentoMapper.toPagamento(criarPagamentoRequest);
        pagamento = pagamentoService.salvarPagamento(pagamento);

        return pagamentoMapper.toCriarPagamentoResponse(pagamento);
    }

    @GetMapping("/pagamentos/{idCartao}")
    @ResponseStatus(HttpStatus.OK)
    public List<GetPagamentoResponse> listarPagamentosPorIdCartao(@PathVariable int idCartao) {

        return pagamentoMapper.ToGetPagamentoResponse(pagamentoService.listarPagamentosPorIdCartao(idCartao));

    }

    @GetMapping("/pagamentos/pagar/{idCartao}")
    @ResponseStatus(HttpStatus.OK)
    public PagarCartaoResponse efetuarPagamentoDoCartao(@PathVariable int idCartao) {

        try {
            return pagamentoService.pagarCartao(idCartao);
        }catch (RuntimeException ex){
            throw new PagamentoNotFoundException();
        }

    }

    @GetMapping("/chama/cartao/{idCartao}")
    @ResponseStatus(HttpStatus.OK)
    public void testeRibbon(@PathVariable int idCartao) {
        cartaoPagamento.buscarCartaoPorId(idCartao);

        System.out.println("API Pagamento chamando API Cartao -> " + "ID: " + LocalDateTime.now().toString());
    }


}
