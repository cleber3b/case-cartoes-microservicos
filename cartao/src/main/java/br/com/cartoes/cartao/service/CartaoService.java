package br.com.cartoes.cartao.service;

import br.com.cartoes.cartao.exception.CartaoNotFoundException;
import br.com.cartoes.cartao.models.Cartao;
import br.com.cartoes.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    //@Autowired
    //ClienteRepository clienteRepository;

    public Cartao salvarCartao(Cartao cartao) {
        cartao.setAtivo(false);
        return cartaoRepository.save(cartao);
    }

    public Cartao buscarPorID(int idCartao) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(idCartao);

        if (!optionalCartao.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return optionalCartao.get();
    }

    public Cartao ativarCartaoPeloNumero(Cartao cartao) {
        Cartao byCartao = cartaoRepository.findByNumero(cartao.getNumero());

        if (byCartao == null) {
            throw new CartaoNotFoundException();
        }

        byCartao.setAtivo(cartao.getAtivo());
        return cartaoRepository.save(byCartao);
    }
}
