package br.com.cartoes.cartao.models.dto;

import javax.validation.constraints.NotNull;

public class AtualizarCartaoRequest {

    private Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
