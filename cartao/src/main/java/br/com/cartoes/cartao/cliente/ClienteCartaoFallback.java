package br.com.cartoes.cartao.cliente;

public class ClienteCartaoFallback implements ClienteCartao {

    /*Aqui apos o implements vc define a regra de negócio e ação que vai acontecer
    * quando o serviço de Cliente for chamado e nõa tiver nenhuma resposta
    * */

    @Override
    public Cliente buscarClientePorId(int idCliente) {

        throw new ClienteNotAvaliable();
    }
}
