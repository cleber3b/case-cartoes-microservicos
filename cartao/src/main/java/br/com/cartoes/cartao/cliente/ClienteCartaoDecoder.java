package br.com.cartoes.cartao.cliente;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteCartaoDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new ClienteNotFoundException(); //lança minha exceção
        }
        return errorDecoder.decode(s, response);
        //nesse caso qualquer coisa do erro acima 404 vai assumir como default o erroDecoder do Feign

    }
}
