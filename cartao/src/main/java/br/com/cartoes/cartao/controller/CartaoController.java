package br.com.cartoes.cartao.controller;

import br.com.cartoes.cartao.cliente.Cliente;
import br.com.cartoes.cartao.cliente.ClienteCartao;
import br.com.cartoes.cartao.cliente.ClienteNotFoundException;
import br.com.cartoes.cartao.models.Cartao;
import br.com.cartoes.cartao.models.dto.*;
import br.com.cartoes.cartao.service.CartaoService;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper cartaoMapper;

    @Autowired
    private ClienteCartao clienteCartao;

//    @Bean
//    public MyErrorDecoder myErrorDecoder() {
//        return new MyErrorDecoder();
//    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CriarCartaoResponse cadastrarCartao(@RequestBody @Valid CriarCartaoRequest criarCartaoRequest) {
        Cartao cartao = cartaoMapper.toCartao(criarCartaoRequest);

        try {
            Cliente cliente = new Cliente();
            cliente = clienteCartao.buscarClientePorId(cartao.getClienteId());
        } catch (FeignException.NotFound ex) {
            throw new ClienteNotFoundException();
        }

        //Salva o Cartao
        cartao = cartaoService.salvarCartao(cartao);

        return cartaoMapper.toCriarCartaoResponse(cartao);
    }

    @GetMapping("/{idCartao}")
    @ResponseStatus(HttpStatus.OK)
    public GetCartaoResponse buscarPorID(@PathVariable int idCartao) {

        System.out.println("API CARTAO chamada pela API Pagamento -> " + LocalDateTime.now().toString());

        Cartao cartao = cartaoService.buscarPorID(idCartao);
        return cartaoMapper.toGetCartaoResponse(cartao);

    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public AtualizarCartaoResponse ativarCartaoPeloNumero(@RequestBody @Valid AtualizarCartaoRequest atualizarCartaoRequest, @PathVariable int numero) {
        Cartao cartao = cartaoMapper.toCartao(atualizarCartaoRequest);
        cartao.setNumero(numero);

        cartao = cartaoService.ativarCartaoPeloNumero(cartao);

        return cartaoMapper.toAtualizarCartaoResponse(cartao);
    }

}
