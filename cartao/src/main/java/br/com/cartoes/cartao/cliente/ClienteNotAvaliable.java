package br.com.cartoes.cartao.cliente;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE, reason = "MicroServiço de Cliente fora do ar.")
public class ClienteNotAvaliable extends RuntimeException {
}
