package br.com.cartoes.cartao.cliente;

import com.netflix.client.ClientException;

public class ClienteCartaoFallbackLoadBalance implements ClienteCartao {

    private Exception exception;

    public ClienteCartaoFallbackLoadBalance(Exception exception){
        this.exception = exception;
    }

    @Override
    public Cliente buscarClientePorId(int idCliente) {
        if(exception.getCause() instanceof ClientException){
            throw new ClienteNotAvaliable();
        }
        throw (RuntimeException) exception;

    }
}
