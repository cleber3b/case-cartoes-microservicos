package br.com.cartoes.cartao.cliente;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;

@FeignClient(name = "CLIENTE", configuration = ClienteCartaoConfiguration.class)
public interface ClienteCartao {

    @GetMapping("/cliente/{idCliente}")
    Cliente buscarClientePorId(@PathVariable int idCliente);



}
