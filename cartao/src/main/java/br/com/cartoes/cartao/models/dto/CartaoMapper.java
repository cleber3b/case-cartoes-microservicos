package br.com.cartoes.cartao.models.dto;

import br.com.cartoes.cartao.cliente.Cliente;
import br.com.cartoes.cartao.models.Cartao;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao(CriarCartaoRequest criarCartaoRequest){
        Cliente cliente = new Cliente();
        cliente.setId(criarCartaoRequest.getClienteId());

        Cartao cartao = new Cartao();
        cartao.setNumero(criarCartaoRequest.getNumero());
        cartao.setClienteId(cliente.getId());

        return cartao;
    }

    public Cartao toCartao(AtualizarCartaoRequest atualizarCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(atualizarCartaoRequest.getAtivo());
        return cartao;
    }

    public CriarCartaoResponse toCriarCartaoResponse(Cartao cartao) {
        CriarCartaoResponse criarCartaoResponse = new CriarCartaoResponse();
        criarCartaoResponse.setId(cartao.getId());
        criarCartaoResponse.setNumero(cartao.getNumero());
        criarCartaoResponse.setAtivo(cartao.getAtivo());
        criarCartaoResponse.setClienteId(cartao.getClienteId());
        return criarCartaoResponse;
    }

    public GetCartaoResponse toGetCartaoResponse(Cartao cartao) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();
        getCartaoResponse.setId(cartao.getId());
        getCartaoResponse.setNumero(cartao.getNumero());
        getCartaoResponse.setAtivo(cartao.getAtivo());
        getCartaoResponse.setClienteId(cartao.getClienteId());
        return getCartaoResponse;
    }

    public AtualizarCartaoResponse toAtualizarCartaoResponse(Cartao cartao) {
        AtualizarCartaoResponse atualizarCartaoResponse = new AtualizarCartaoResponse();
        atualizarCartaoResponse.setId(cartao.getId());
        atualizarCartaoResponse.setNumero(cartao.getNumero());
        atualizarCartaoResponse.setAtivo(cartao.getAtivo());
        atualizarCartaoResponse.setClienteId(cartao.getClienteId());
        return atualizarCartaoResponse;
    }
}
