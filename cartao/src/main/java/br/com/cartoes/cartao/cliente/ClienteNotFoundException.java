package br.com.cartoes.cartao.cliente;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente Não encontrado.")
public class ClienteNotFoundException extends RuntimeException {
}
