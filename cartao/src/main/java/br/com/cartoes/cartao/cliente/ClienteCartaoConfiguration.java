package br.com.cartoes.cartao.cliente;

import com.netflix.client.ClientException;
import com.netflix.loadbalancer.LoadBalancerContext;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;

public class ClienteCartaoConfiguration {

    @Bean
    public ErrorDecoder getClienteCartaoDecoder(){
        return new ClienteCartaoDecoder();
    }

    @Bean
    public Feign.Builder builder(){
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new ClienteCartaoFallback(), RetryableException.class)
                .withFallbackFactory(ClienteCartaoFallbackLoadBalance::new, RuntimeException.class)

                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
