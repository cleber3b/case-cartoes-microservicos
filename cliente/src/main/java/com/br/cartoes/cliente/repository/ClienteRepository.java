package com.br.cartoes.cliente.repository;

import com.br.cartoes.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

    Cliente findById(int id);

}
