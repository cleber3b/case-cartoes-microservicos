package com.br.cartoes.cliente.exception;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteConfiguration {

    @Bean
    public ErrorDecoder getClienteDecoder(){
        return new ClienteDecoder();
    }

}
