package com.br.cartoes.cliente.controller;

import com.br.cartoes.cliente.models.Cliente;
import com.br.cartoes.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente){
        return clienteService.salvarCliente(cliente);
    }

    @GetMapping("/{idCliente}")
    @ResponseStatus(HttpStatus.OK)
    public Cliente buscarPorID(@PathVariable(name = "idCliente") int idCliente){
        return clienteService.buscarPorID(idCliente);
    }



}
