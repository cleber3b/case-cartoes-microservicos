package com.br.cartoes.cliente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.ribbon.RibbonClients;

import java.time.LocalDateTime;

@SpringBootApplication
@RibbonClients(defaultConfiguration = RibbonConfiguration.class) //Aqui estou dizenod qual vai ser a distribuição de carga usando o Ribbon
public class ClienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClienteApplication.class, args);
	}



}
