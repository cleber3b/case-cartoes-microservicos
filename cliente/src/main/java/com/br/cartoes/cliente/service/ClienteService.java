package com.br.cartoes.cliente.service;

import com.br.cartoes.cliente.exception.ClienteNotFoundException;
import com.br.cartoes.cliente.models.Cliente;
import com.br.cartoes.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente buscarPorID(int idCliente) {
        Cliente cliente = clienteRepository.findById(idCliente);

        if(cliente != null){
            return cliente;
        }else {
            throw new ClienteNotFoundException();
        }
    }
}
