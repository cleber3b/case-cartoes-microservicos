package com.br.cartoes.fatura.externalMicroservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE")
public interface ClienteFatura {

    @GetMapping("/cliente/{idCliente}")
    Cliente buscarClientePorId(@PathVariable int idCliente);
}
