package com.br.cartoes.fatura.controller;

import com.br.cartoes.fatura.exception.FaturaNotFoundException;
import com.br.cartoes.fatura.externalMicroservice.*;
import com.br.cartoes.fatura.models.Fatura;
import com.br.cartoes.fatura.models.dto.ExpirarCartaoFaturaResponse;
import com.br.cartoes.fatura.models.dto.FaturaMapper;
import com.br.cartoes.fatura.models.dto.GetFaturaResponse;
import com.br.cartoes.fatura.models.dto.PagarFaturaResponse;
import com.br.cartoes.fatura.service.FaturaService;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    FaturaService faturaService;

    @Autowired
    CartaoFatura cartaoFatura;

    @Autowired
    ClienteFatura clienteFatura;

    @Autowired
    PagamentoFatura pagamentoFatura;

    @Autowired
    FaturaMapper faturaMapper;

    @GetMapping("/{clienteId}/{cartaoId}")
    @ResponseStatus(HttpStatus.OK)
    public List<GetFaturaResponse> exibirFatura(@PathVariable int clienteId, @PathVariable int cartaoId) {

        try {
            Cliente cliente = clienteFatura.buscarClientePorId(clienteId);

            Cartao cartao = cartaoFatura.buscarCartaoPorID(cartaoId);

            if (cliente.getId() != cartao.getClienteId()) {

                throw new RuntimeException("Cliente não pertence ao Cartão Informado");
                //lançar uma excepiton dizendo que Cliente ou Cartão Invalido
                //Cliente não pertence ao Cartão Informado
                //Cartão não pertence ao Cliente Informado
            }

            return pagamentoFatura.listarPagamentosPorIdCartao(cartao.getId());

        } catch (FeignException.NotFound ex) {
            throw new FaturaNotFoundException();
        }
    }

    @PostMapping("/{clienteId}/{cartaoId}/pagar")
    @ResponseStatus(HttpStatus.OK)
    public PagarFaturaResponse pagarFatura(@PathVariable int clienteId, @PathVariable int cartaoId) {

        Cliente cliente = clienteFatura.buscarClientePorId(clienteId); //validar cliente

        Cartao cartao = cartaoFatura.buscarCartaoPorID(cartaoId); //validar cartao

        if (cliente.getId() != cartao.getClienteId()) {

            throw new RuntimeException("Cliente não pertence ao Cartão Informado");
            //lançar uma excepiton dizendo que Cliente ou Cartão Invalido
            //Cliente não pertence ao Cartão Informado
            //Cartão não pertence ao Cliente Informado
        }

        //Até aqui esta funcionando
        //PagarFaturaResponse pagarFaturaResponse = pagamentoFatura.efetuarPagamentoDoCartao(cartao.getId());
        PagarFaturaResponse pagarFaturaResponse = new PagarFaturaResponse();
        pagarFaturaResponse.setValorPago(12.00);
        pagarFaturaResponse.setPagoEm(LocalDate.now());

        Fatura fatura = faturaMapper.toFatura(pagarFaturaResponse, cartao);

        try {
            fatura = faturaService.pagarFatura(fatura);

            return faturaMapper.toPagarFaturaResponse(fatura);

        } catch (RuntimeException ex) {
            throw new RuntimeException("Erro inesperado: " + ex.getMessage());
        }
    }

    @PatchMapping("/{clienteId}/{cartaoId}/expirar")
    @ResponseStatus(HttpStatus.OK)
    public ExpirarCartaoFaturaResponse expirarCartaoFatura(@PathVariable int clienteId, @PathVariable int cartaoId) {

        Cliente cliente = clienteFatura.buscarClientePorId(clienteId); //validar cliente

        Cartao cartao = cartaoFatura.buscarCartaoPorID(cartaoId); //validar cartao

        if (cliente.getId() != cartao.getClienteId()) {

            throw new RuntimeException("Cliente não pertence ao Cartão Informado");
            //lançar uma excepiton dizendo que Cliente ou Cartão Invalido
            //Cliente não pertence ao Cartão Informado
            //Cartão não pertence ao Cliente Informado
        }

        try {

            cartao = cartaoFatura.expirarCartaoPeloNumero(faturaMapper.toExpirarCartaoRequest(), cartao.getNumero());

        } catch (RuntimeException ex) {
            throw new RuntimeException("Erro inesperado: " + ex.getMessage());
        }

        return faturaMapper.toExpirarCartaoFaturaResponse();
    }

}
