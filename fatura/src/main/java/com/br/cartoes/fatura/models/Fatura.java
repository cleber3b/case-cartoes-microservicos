package com.br.cartoes.fatura.models;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "faturas")
public class Fatura {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Valor não pode ser nulo ou vazio.")
    @DecimalMin(value = "0.1", message = "Valor precisa ser maior que R$1.00.")
    @Digits(integer = 6, fraction = 2, message = "Valor fora do padrão.")
    private Double valorPago;

    @DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss", iso = DateTimeFormat.ISO.DATE)
    private LocalDate pagoEm;

    private int cartaoId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    public LocalDate getPagoEm() {
        return pagoEm;
    }

    public void setPagoEm(LocalDate pagoEm) {
        this.pagoEm = pagoEm;
    }

    public int getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(int cartaoId) {
        this.cartaoId = cartaoId;
    }
}
