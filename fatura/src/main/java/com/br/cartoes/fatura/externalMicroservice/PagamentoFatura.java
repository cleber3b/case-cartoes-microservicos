package com.br.cartoes.fatura.externalMicroservice;

import com.br.cartoes.fatura.models.dto.GetFaturaResponse;
import com.br.cartoes.fatura.models.dto.PagarFaturaResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "PAGAMENTO")
public interface PagamentoFatura {

    @GetMapping("/pagamentos/{idCartao}")
    List<GetFaturaResponse> listarPagamentosPorIdCartao(@PathVariable int idCartao);

    @GetMapping("/pagamentos/pagar/{idCartao}")
    PagarFaturaResponse efetuarPagamentoDoCartao(@PathVariable int idCartao);



}
