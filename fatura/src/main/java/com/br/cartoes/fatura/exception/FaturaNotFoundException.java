package com.br.cartoes.fatura.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Não foram encontrados movimentações para exibir a Fatura.")
public class FaturaNotFoundException extends RuntimeException {

}
