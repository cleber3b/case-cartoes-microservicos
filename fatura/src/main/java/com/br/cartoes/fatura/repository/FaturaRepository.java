package com.br.cartoes.fatura.repository;

import com.br.cartoes.fatura.models.Fatura;
import org.springframework.data.repository.CrudRepository;

public interface FaturaRepository extends CrudRepository<Fatura, Integer> {

}
