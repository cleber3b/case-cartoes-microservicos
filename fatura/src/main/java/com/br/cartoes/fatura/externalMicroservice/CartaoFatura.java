package com.br.cartoes.fatura.externalMicroservice;

import com.br.cartoes.fatura.models.dto.ExpirarCartaoRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "CARTAO")
public interface CartaoFatura {

    @GetMapping("/cartao/{idCartao}")
    Cartao buscarCartaoPorID(@PathVariable int idCartao);

    @PatchMapping("/cartao/{numero}")
    Cartao expirarCartaoPeloNumero(@RequestBody ExpirarCartaoRequest expirarCartaoRequest, @PathVariable int numero);
}
