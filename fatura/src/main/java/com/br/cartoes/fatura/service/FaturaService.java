package com.br.cartoes.fatura.service;

import com.br.cartoes.fatura.models.Fatura;
import com.br.cartoes.fatura.repository.FaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FaturaService {

    @Autowired
    FaturaRepository faturaRepository;

    public Fatura pagarFatura(Fatura fatura) {
        return faturaRepository.save(fatura);
    }

}
