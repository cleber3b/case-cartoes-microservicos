package com.br.cartoes.fatura.models.dto;

import com.br.cartoes.fatura.externalMicroservice.Cartao;
import com.br.cartoes.fatura.models.Fatura;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FaturaMapper {

    public List<GetFaturaResponse> ToGetPagamentoResponse(List<Fatura> listaDeFatura) {

        List<GetFaturaResponse> listaGetFaturaResponse = new ArrayList<>();
        GetFaturaResponse getFaturaResponse;

        for (Fatura fatura : listaDeFatura) {
            getFaturaResponse = new GetFaturaResponse();

            getFaturaResponse.setId(fatura.getId());
            getFaturaResponse.setCartaoId(fatura.getCartaoId());


            listaGetFaturaResponse.add(getFaturaResponse);
        }

        return listaGetFaturaResponse;
    }

    public PagarFaturaResponse toPagarFaturaResponse(Fatura fatura){
        PagarFaturaResponse pagarFaturaResponse = new PagarFaturaResponse();
        pagarFaturaResponse.setId(fatura.getId());
        pagarFaturaResponse.setPagoEm(fatura.getPagoEm());
        pagarFaturaResponse.setValorPago(fatura.getValorPago());

        return pagarFaturaResponse;
    }

    public ExpirarCartaoFaturaResponse toExpirarCartaoFaturaResponse(){
        ExpirarCartaoFaturaResponse expirarCartaoFaturaResponse = new ExpirarCartaoFaturaResponse();
        expirarCartaoFaturaResponse.setStatus("ok");

        return expirarCartaoFaturaResponse;
    }

    public ExpirarCartaoRequest toExpirarCartaoRequest(){
        ExpirarCartaoRequest expirarCartaoRequest = new ExpirarCartaoRequest();
        expirarCartaoRequest.setAtivo(false);

        return expirarCartaoRequest;
    }

    public Fatura toFatura(PagarFaturaResponse pagarFaturaResponse, Cartao cartao) {
        Fatura fatura = new Fatura();

        fatura.setPagoEm(pagarFaturaResponse.getPagoEm());
        fatura.setValorPago(pagarFaturaResponse.getValorPago());
        fatura.setCartaoId(cartao.getId());

        return fatura;
    }
}
