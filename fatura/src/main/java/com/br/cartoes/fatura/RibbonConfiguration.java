package com.br.cartoes.fatura;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfiguration {

    @Bean
    public IRule getRule(){
        return new RandomRule();
        //vai distribuir as requisições quando for chamado o Cliente entre as N instancias do microserviço
        //que estiverem de pé no Eukreka.
        //Primeira requisição vai pro primeiro servidor
        //Segunda requisição vai par ao segundo servidor
        //Terceira requisição para para o primeiro servidor e por ai vai...
        //Nesse caso se tiver apenas 2 servidores rodando.
    }
}
